import discord
from discord.ext import commands
import mysql.connector

#6 rijen en 7 kolommen

class Connect4(commands.Cog):
    def __init__(self, client):
        self.client = client

    def connectDB(self):
        mydb = mysql.connector.connect(
            host="localhost",
            user="root",
            password="",
            database="practice_enterprise_bot"
        )
        return mydb

    db_tabel_used = "connect4"

    aantalRows = 6
    aantalKolommen = 7
    yellowChar = ":yellow_circle:"
    redChar = ":red_circle:"
    borderChar = ":white_large_square:"
    fieldChar = ":black_large_square:"

    playChar = ""
    board = []
    printboard = []
    gameAlreadyPlaying = False
    player1 = ""
    player2 = ""
    turn = ""
    message = None
    validcoordinates = ['1', '2', '3', '4', '5', '6', '7']

    @commands.command(aliases=['4'])
    async def connect4(self, ctx, p2 : discord.Member = None):

        if self.gameAlreadyPlaying:
            await ctx.channel.send(f'{self.player1} and {self.player2} are already playing, wait for them to finish!')
            return

        if p2 is None:
            await ctx.channel.send("Enter a user to play with:  .connect4 @name or .4 @name")
            return

        if p2 == ctx.author:
            await ctx.channel.send(f'You cant play against yourself!')
            return

        self.player1 = ctx.author
        self.player2 = p2
        self.turn = self.player1

        await ctx.channel.send(f'Use the command .connect4_place or .4_p to make a move.\n\tExample: .4_p 3\nTo reset the game use command: .connect4_reset or .4_r')

        self.board = self.create_board()
        self.printboard = self.array_print_board(self.board)
        self.message = await ctx.channel.send(self.string_print_board(self.printboard))

        self.check_players_in_db(self.db_tabel_used)

    @commands.command(aliases=['4_r'])
    async def connect4_reset(self, ctx):
        await self.message.delete()
        await ctx.channel.send(f'Game has been reset!')
        self.reset_game()

    @commands.command(aliases=['4_p'])
    async def connect4_place(self, ctx, coordinates=None):
        if self.gameAlreadyPlaying is False:
            return

        if coordinates not in self.validcoordinates:
            await ctx.channel.send(f'Enter a valid coordinate')
            return

        if ctx.author != self.player1 and ctx.author != self.player2:
            await ctx.channel.send(f'You are not a player. Only {self.player1} or {self.player2} can make a move!')
            return

        if ctx.author != self.turn:
            await ctx.channel.send(f'Its {self.turn} his turn')
            return

        if self.turn == ctx.author:
            if self.turn == self.player1:
                self.playChar = self.yellowChar
                self.turn = self.player2
            else:
                self.playChar = self.redChar
                self.turn = self.player1
        zet = self.coordinates_to_zet(coordinates)
        if self.illegal_move(zet):
            await ctx.channel.send(f'Illegal move, try again')
            return
        self.board[zet] = self.playChar
        self.printboard = self.array_print_board(self.board)
        await self.message.delete()
        self.message = await ctx.channel.send(self.string_print_board(self.printboard))

        if self.check_for_tie():
            await ctx.channel.send(f'Game has ended in a tie!')

            self.update_db(self.player1.id, "ties", self.db_tabel_used)
            self.update_db(self.player2.id, "ties", self.db_tabel_used)

            self.reset_game()
            return

        if self.winconditions_met(zet):
            if self.turn == self.player1:
                await ctx.channel.send(f':trophy: {self.player2} won!')

                self.update_db(self.player2.id, "wins", self.db_tabel_used)
                self.update_db(self.player1.id, "loses", self.db_tabel_used)

            else:
                await ctx.channel.send(f':trophy: {self.player1} won!')

                self.update_db(self.player1.id, "wins", self.db_tabel_used)
                self.update_db(self.player2.id, "loses", self.db_tabel_used)

            self.reset_game()


    def create_board(self):
        temp_board = []

        for i in range(self.aantalRows * self.aantalKolommen):
            temp_board.append(':black_large_square:')
            self.gameAlreadyPlaying = True
        return temp_board

    def array_print_board(self, board):
        indexNumbers = ['\n', self.borderChar, ':one:', ':two:', ':three:', ':four:', ':five:', ':six:', ':seven:', self.borderChar]
        printBoard = []
        for j in range(9):
            printBoard.append(self.borderChar)
        printBoard.append('\n')
        printBoard.append(self.borderChar)

        for i in range(len(self.board)):
            if i == 7 or i == 14 or i == 21 or i == 28 or i == 35:
                printBoard.append(self.borderChar)
                printBoard.append('\n')
                printBoard.append(self.borderChar)
            printBoard.append(board[i])
        printBoard.append(self.borderChar)

        for k in range(10):
            printBoard.append(indexNumbers[k])

        printBoard.append(f'\n{self.player1} = yellow\n{self.player2} = red')

        return printBoard

    def string_print_board(self, printboard):
        printString = ""
        for i in range(len(printboard)):
            printString += printboard[i]

        return printString

    def coordinates_to_zet(self, coordinates):

        # laten vallen tot beneden

        if coordinates == '1':
            x = 35
            for i in range(6):
                if(self.board[x]) == self.fieldChar:
                    return x
                else:
                    x -= 7
            return 999

        elif coordinates == '2':
            x = 36
            for i in range(6):
                if (self.board[x]) == self.fieldChar:
                    return x
                else:
                    x -= 7
            return 999

        elif coordinates == '3':
            x = 37
            for i in range(6):
                if (self.board[x]) == self.fieldChar:
                    return x
                else:
                    x -= 7
            return 999

        elif coordinates == '4':
            x = 38
            for i in range(6):
                if (self.board[x]) == self.fieldChar:
                    return x
                else:
                    x -= 7
            return 999

        elif coordinates == '5':
            x = 39
            for i in range(6):
                if (self.board[x]) == self.fieldChar:
                    return x
                else:
                    x -= 7
            return 999

        elif coordinates == '6':
            x = 40
            for i in range(6):
                if (self.board[x]) == self.fieldChar:
                    return x
                else:
                    x -= 7
            return 999

        elif coordinates == '7':
            x = 41
            for i in range(6):
                if (self.board[x]) == self.fieldChar:
                    return x
                else:
                    x -= 7
            return 999

        else:
            return 999

    def winconditions_met(self, zet):

        # 0 - 3 && 7 - 10 && 14 - 17 && 21 - 24 && 28 - 31 && 35 - 38
        if zet >= 0 and zet <= 3 or zet >= 7 and zet <= 10 or zet >= 14 and zet <= 17 or zet >= 21 and zet <= 24 or zet >= 28 and zet <= 31 or zet >= 35 and zet <= 38:
            # Horizontaal: 0L_3R
            if self.board[zet+1] == self.board[zet] and self.board[zet+2] == self.board[zet] and self.board[zet+3] == self.board[zet]:
                return True
        # 1 - 4 && 8 - 11 && 15 - 18 && 22 - 25 && 29 - 32 && 36 - 39
        if zet >= 1 and zet <= 4 or zet >= 8 and zet <= 11 or zet >= 15 and zet <= 18 or zet >= 22 and zet <= 25 or zet >= 29 and zet <= 32 or zet >= 36 and zet <= 39:
            # Horizontaal: 1L_2R
            if self.board[zet-1] == self.board[zet] and self.board[zet+1] == self.board[zet] and self.board[zet+2] == self.board[zet]:
                return True
        # 2 - 5 && 9 - 12 && 16 - 19 && 23- 26 && 30 - 33 && 37 - 40
        if zet >= 2 and zet <= 5 or zet >= 9 and zet <= 12 or zet >= 16 and zet <= 19 or zet >= 23 and zet <= 26 or zet >= 30 and zet <= 33 or zet >= 37 and zet <= 40:
            # Horizontaal: 2L_1R
            if self.board[zet-2] == self.board[zet] and self.board[zet-1] == self.board[zet] and self.board[zet+1] == self.board[zet]:
                return True
        # 3 - 6 && 10 - 13 && 17 - 20 && 24- 27 && 31 - 34 && 38 - 41
        if zet >= 3 and zet <= 6 or zet >= 10 and zet <= 13 or zet >= 17 and zet <= 20 or zet >= 24 and zet <= 27 or zet >= 31 and zet <= 34 or zet >= 38 and zet <= 41:
            # Horizontaal: 2L_1R
            if self.board[zet-3] == self.board[zet] and self.board[zet-2] == self.board[zet] and self.board[zet-1] == self.board[zet]:
                return True

        # 21 - 41
        if zet >= 21 and zet <= 41:
            # Verticaal: 0ond_3B
            if self.board[zet-21] == self.board[zet] and self.board[zet-14] == self.board[zet] and self.board[zet-7] == self.board[zet]:
                return True
        # 14 - 34
        if zet >= 14 and zet <= 34:
            # Verticaal: 1ond_2B
            if self.board[zet-14] == self.board[zet] and self.board[zet-7] == self.board[zet] and self.board[zet+7] == self.board[zet]:
                return True
        # 7 - 27
        if zet >= 7 and zet <= 27:
            # Verticaal: 2ond_1B
            if self.board[zet-7] == self.board[zet] and self.board[zet+7] == self.board[zet] and self.board[zet+14] == self.board[zet]:
                return True
        # 0 - 20
        if zet >= 0 and zet <= 20:
            # Verticaal: 3ond_0B
            if self.board[zet+7] == self.board[zet] and self.board[zet+14] == self.board[zet] and self.board[zet+21] == self.board[zet]:
                return True

        # 21 - 24 && 28 - 31 && 35 - 38
        if zet >= 21 and zet <= 24 or zet >= 28 and zet <= 31 or zet >= 35 and zet <= 38:
            # Schuin (/): 0L-ond_3R-B
            if self.board[zet-6] == self.board[zet] and self.board[zet-12] == self.board[zet] and self.board[zet-18] == self.board[zet]:
                return True
        # 15 - 18 && 22 - 25 && 39 - 32
        if zet >= 15 and zet <= 18 or zet >= 22 and zet <= 25 or zet >= 29 and zet <= 32:
            # Schuin (/): 1L-ond_2R-B
            if self.board[zet+6] == self.board[zet] and self.board[zet-6] == self.board[zet] and self.board[zet-12] == self.board[zet]:
                return True
        # 9 - 12 && 16 - 19 && 23 - 32
        if zet >= 9 and zet <= 12 or zet >= 16 and zet <= 19 or zet >= 23 and zet <= 26:
            # Schuin (/): 2L-ond_1R-B
            if self.board[zet+12] == self.board[zet] and self.board[zet+6] == self.board[zet] and self.board[zet-6] == self.board[zet]:
                return True
        # 3 - 6 && 10 - 13 && 17 - 20
        if zet >= 3 and zet <= 6 or zet >= 10 and zet <= 13 or zet >= 17 and zet <= 20:
            # Schuin (/): 3L-ond_0R-B
            if self.board[zet+18] == self.board[zet] and self.board[zet+12] == self.board[zet] and self.board[zet+6] == self.board[zet]:
                return True

        # 0 - 3 && 7 - 10 && 14 - 17
        if zet >= 0 and zet <= 3 or zet >= 7 and zet <= 10 or zet >= 14 and zet <= 17:
            # Schuin (\): 0L-B_3R-ond
            if self.board[zet+8] == self.board[zet] and self.board[zet+16] == self.board[zet] and self.board[zet+24] == self.board[zet]:
                return True
        # 8 - 11 && 15 - 18 && 22 - 25
        if zet >= 8 and zet <= 11 or zet >= 15 and zet <= 18 or zet >= 22 and zet <= 25:
            # Schuin (\): 1L-B_2R-ond
            if self.board[zet-8] == self.board[zet] and self.board[zet+8] == self.board[zet] and self.board[zet+16] == self.board[zet]:
                return True
        # 16 - 19 && 23 - 26 && 30 - 33
        if zet >= 16 and zet <= 19 or zet >= 23 and zet <= 26 or zet >= 30 and zet <= 33:
            # Schuin (\): 2L-B_1R-ond
            if self.board[zet-16] == self.board[zet] and self.board[zet-8] == self.board[zet] and self.board[zet+8] == self.board[zet]:
                return True
        # 24 - 27 && 31 - 34 && 38 - 41
        if zet >= 24 and zet <= 27 or zet >= 31 and zet <= 34 or zet >= 38 and zet <= 41:
            # Schuin (\): 3L-B_0R-ond
            if self.board[zet-24] == self.board[zet] and self.board[zet-16] == self.board[zet] and self.board[zet-8] == self.board[zet]:
                return True

        return False

    def check_for_tie(self):
        check = 0
        for i in range(len(self.board)):
            if self.board[i] != ':black_large_square:':
                check += 1
        if check == len(self.board):
            return True

    def reset_game(self):

        self.playChar = ""
        self.board = []
        self.printboard = []
        self.gameAlreadyPlaying = False
        self.player1 = ""
        self.player2 = ""
        self.turn = ""
        self.message = None

    def illegal_move(self, zet):
        if zet == 999:
            if self.turn == self.player1:
                self.turn = self.player2
            else:
                self.turn = self.player1
            return True
        if self.board[zet] != ':black_large_square:' or zet == 999:
            if self.turn == self.player1:
                self.turn = self.player2
            else:
                self.turn = self.player1
            return True

    def update_db(self, player, db_kolom, db_tabel):

        mydb = self.connectDB()
        my_cursor = mydb.cursor()
        my_sql = f"SELECT {db_kolom} FROM {db_tabel} WHERE user_id = {player}"
        my_cursor.execute(my_sql)

        myresult = my_cursor.fetchall()[0][0]
        update_player = f"UPDATE {db_tabel} SET {db_kolom} = {myresult + 1} WHERE user_id = {player}"
        my_cursor.execute(update_player)
        mydb.commit()
        my_cursor.close()
        mydb.close()

    def check_players_in_db(self, db_tabel):
        mydb = self.connectDB()
        my_cursor = mydb.cursor()
        my_player = self.player1.id
        my_sql = f"SELECT user_id FROM {db_tabel} WHERE user_id = {my_player}"
        my_cursor.execute(my_sql)
        myresult = my_cursor.fetchall()
        if myresult == []:
            no_player = f"INSERT INTO {db_tabel} (user_id, wins, loses, ties) VALUES (%s, %s, %s, %s)"
            record = (my_player, 0, 0, 0)
            my_cursor.execute(no_player, record)
            mydb.commit()

        my_player = self.player2.id
        my_sql = f"SELECT user_id FROM {db_tabel} WHERE user_id = {my_player}"
        my_cursor.execute(my_sql)
        myresult = my_cursor.fetchall()
        if myresult == []:
            no_player = f"INSERT INTO {db_tabel} (user_id, wins, loses, ties) VALUES (%s, %s, %s, %s)"
            record = (my_player, 0, 0, 0)
            my_cursor.execute(no_player, record)
            mydb.commit()
        my_cursor.close()
        mydb.close()



def setup(client):
    client.add_cog(Connect4(client))