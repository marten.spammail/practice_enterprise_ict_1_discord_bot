import discord
from discord.ext import commands
import random
import os

class Commands(commands.Cog):
    def __init__(self, client):
        self.client = client

    @commands.command()
    async def help_leaderboard(self, ctx):
        await self.send_message(f"Info about the leaderboard commands Page 1/2",
                                f'.leaderboard\n'
                                f':regional_indicator_i::regional_indicator_n::regional_indicator_f::regional_indicator_o:\n'
                                f'Gives top 3 player for all games combined\n'
                                f':regional_indicator_c::regional_indicator_o::regional_indicator_m::regional_indicator_m::regional_indicator_e::regional_indicator_n::regional_indicator_t::regional_indicator_s:\n'
                                f'/\n'
                                f':a::regional_indicator_l::regional_indicator_i::regional_indicator_a::regional_indicator_s:\n'
                                f'.lb\n.stats\n\n'

                                f'.leaderboard_connect4 @name\n'
                                f':regional_indicator_i::regional_indicator_n::regional_indicator_f::regional_indicator_o:\n'
                                f'Gives stats for the mentioned user for the game connect4\n'
                                f':regional_indicator_c::regional_indicator_o::regional_indicator_m::regional_indicator_m::regional_indicator_e::regional_indicator_n::regional_indicator_t::regional_indicator_s:\n'
                                f'If no "@name" is given the author of the message wil be chosen\n'
                                f':a::regional_indicator_l::regional_indicator_i::regional_indicator_a::regional_indicator_s:\n'
                                f'.leaderboard_4\n.lb_4\n.stats_connect4\n.stats_4\n\n'

                                f'.leaderboard_oxo @name\n'
                                f':regional_indicator_i::regional_indicator_n::regional_indicator_f::regional_indicator_o:\n'
                                f'Gives stats for the mentioned user for the game oxo\n'
                                f':regional_indicator_c::regional_indicator_o::regional_indicator_m::regional_indicator_m::regional_indicator_e::regional_indicator_n::regional_indicator_t::regional_indicator_s:\n'
                                f'If no @name is given the author of the message wil be chosen\n'
                                f':a::regional_indicator_l::regional_indicator_i::regional_indicator_a::regional_indicator_s:\n'
                                f'.lb_oxo\n.stats_oxo\n\n'
                                , ctx)

        await self.send_message(f"Info about the leaderboard commands Page 2/2",

                                f'.leaderboard_reset_connect4\n'
                                f':regional_indicator_i::regional_indicator_n::regional_indicator_f::regional_indicator_o:\n'
                                f'Reset stats for the game connect4 for everyone\n'
                                f':regional_indicator_c::regional_indicator_o::regional_indicator_m::regional_indicator_m::regional_indicator_e::regional_indicator_n::regional_indicator_t::regional_indicator_s:\n'
                                f'/\n'
                                f':a::regional_indicator_l::regional_indicator_i::regional_indicator_a::regional_indicator_s:\n'
                                f'/\n\n'

                                f'.leaderboard_reset_oxo\n'
                                f':regional_indicator_i::regional_indicator_n::regional_indicator_f::regional_indicator_o:\n'
                                f'Reset stats for the game oxo for everyone\n'
                                f':regional_indicator_c::regional_indicator_o::regional_indicator_m::regional_indicator_m::regional_indicator_e::regional_indicator_n::regional_indicator_t::regional_indicator_s:\n'
                                f'/\n'
                                f':a::regional_indicator_l::regional_indicator_i::regional_indicator_a::regional_indicator_s:\n'
                                f'/\n\n'
                                , ctx)

    @commands.command()
    async def help_connect4(self, ctx):
        await self.send_message(f"Info about the connect4 commands Page 1/1",
                                f'.connect4 @name\n'
                                f':regional_indicator_i::regional_indicator_n::regional_indicator_f::regional_indicator_o:\n'
                                f'Starts a game of connect4 with @name as opponent\n'
                                f':regional_indicator_c::regional_indicator_o::regional_indicator_m::regional_indicator_m::regional_indicator_e::regional_indicator_n::regional_indicator_t::regional_indicator_s:\n'
                                f'/\n'
                                f':a::regional_indicator_l::regional_indicator_i::regional_indicator_a::regional_indicator_s:\n'
                                f'.4\n\n'

                                f'.connect4_place "number"\n'
                                f':regional_indicator_i::regional_indicator_n::regional_indicator_f::regional_indicator_o:\n'
                                f'Places your piece in column "number"\n'
                                f':regional_indicator_c::regional_indicator_o::regional_indicator_m::regional_indicator_m::regional_indicator_e::regional_indicator_n::regional_indicator_t::regional_indicator_s:\n'
                                f'/\n'
                                f':a::regional_indicator_l::regional_indicator_i::regional_indicator_a::regional_indicator_s:\n'
                                f'.4_p\n\n'

                                f'.connect4_reset\n'
                                f':regional_indicator_i::regional_indicator_n::regional_indicator_f::regional_indicator_o:\n'
                                f'Resets the game connect4\n'
                                f':regional_indicator_c::regional_indicator_o::regional_indicator_m::regional_indicator_m::regional_indicator_e::regional_indicator_n::regional_indicator_t::regional_indicator_s:\n'
                                f'/\n'
                                f':a::regional_indicator_l::regional_indicator_i::regional_indicator_a::regional_indicator_s:\n'
                                f'.4_r\n\n'
                                , ctx)

    @commands.command()
    async def help_oxo(self, ctx):
        await self.send_message(f"Info about the oxo commands Page 1/1",
                                f'.oxo @name\n'
                                f':regional_indicator_i::regional_indicator_n::regional_indicator_f::regional_indicator_o:\n'
                                f'Starts a game of oxo with @name as opponent\n'
                                f':regional_indicator_c::regional_indicator_o::regional_indicator_m::regional_indicator_m::regional_indicator_e::regional_indicator_n::regional_indicator_t::regional_indicator_s:\n'
                                f'/\n'
                                f':a::regional_indicator_l::regional_indicator_i::regional_indicator_a::regional_indicator_s:\n'
                                f'/\n\n'

                                f'.oxo_place "coordinate"\n'
                                f':regional_indicator_i::regional_indicator_n::regional_indicator_f::regional_indicator_o:\n'
                                f'Places your piece at the given coordinate\n'
                                f':regional_indicator_c::regional_indicator_o::regional_indicator_m::regional_indicator_m::regional_indicator_e::regional_indicator_n::regional_indicator_t::regional_indicator_s:\n'
                                f'/\n'
                                f':a::regional_indicator_l::regional_indicator_i::regional_indicator_a::regional_indicator_s:\n'
                                f'.oxo_p\n\n'

                                f'.oxo_reset\n'
                                f':regional_indicator_i::regional_indicator_n::regional_indicator_f::regional_indicator_o:\n'
                                f'Resets the game oxo\n'
                                f':regional_indicator_c::regional_indicator_o::regional_indicator_m::regional_indicator_m::regional_indicator_e::regional_indicator_n::regional_indicator_t::regional_indicator_s:\n'
                                f'/\n'
                                f':a::regional_indicator_l::regional_indicator_i::regional_indicator_a::regional_indicator_s:\n'
                                f'.oxo_r\n\n'
                                , ctx)

    @commands.command()
    async def help_clear(self, ctx):
        await self.send_message(f"Info about the clear command Page 1/1",
                                f'.clear "amount"\n'
                                f':regional_indicator_i::regional_indicator_n::regional_indicator_f::regional_indicator_o:\n'
                                f'Deletes the given amount of messages\n'
                                f':regional_indicator_c::regional_indicator_o::regional_indicator_m::regional_indicator_m::regional_indicator_e::regional_indicator_n::regional_indicator_t::regional_indicator_s:\n'
                                f'If no amount is given, 2 is taken. -> your .clear command and the previous message\n'
                                f':a::regional_indicator_l::regional_indicator_i::regional_indicator_a::regional_indicator_s:\n'
                                f'/\n\n'
                                , ctx)

    @commands.command()
    async def help_eightball(self, ctx):
        await self.send_message(f"Info about the eightball command Page 1/1",
                                f'.eightball "question"\n'
                                f':regional_indicator_i::regional_indicator_n::regional_indicator_f::regional_indicator_o:\n'
                                f'Gives a reply to your "question"\n'
                                f':regional_indicator_c::regional_indicator_o::regional_indicator_m::regional_indicator_m::regional_indicator_e::regional_indicator_n::regional_indicator_t::regional_indicator_s:\n'
                                f'/\n'
                                f':a::regional_indicator_l::regional_indicator_i::regional_indicator_a::regional_indicator_s:\n'
                                f'.8ball\n\n'
                                , ctx)

    @commands.command()
    async def help_ping(self, ctx):
        await self.send_message(f"Info about the ping command Page 1/1",
                                f'.ping\n'
                                f':regional_indicator_i::regional_indicator_n::regional_indicator_f::regional_indicator_o:\n'
                                f'Gives your latency to the server in ms\n'
                                f':regional_indicator_c::regional_indicator_o::regional_indicator_m::regional_indicator_m::regional_indicator_e::regional_indicator_n::regional_indicator_t::regional_indicator_s:\n'
                                f'/\n'
                                f':a::regional_indicator_l::regional_indicator_i::regional_indicator_a::regional_indicator_s:\n'
                                f'/\n\n'
                                , ctx)

    @commands.command()
    async def help_votekick(self, ctx):
        await self.send_message(f"Info about the votekick command Page 1/1",
                                f'.votekick @name\n'
                                f':regional_indicator_i::regional_indicator_n::regional_indicator_f::regional_indicator_o:\n'
                                f'Starts a vote to kick @name from the voice channel\n'
                                f':regional_indicator_c::regional_indicator_o::regional_indicator_m::regional_indicator_m::regional_indicator_e::regional_indicator_n::regional_indicator_t::regional_indicator_s:\n'
                                f'Vote lasts vor 15 seconds\n'
                                f':a::regional_indicator_l::regional_indicator_i::regional_indicator_a::regional_indicator_s:\n'
                                f'/\n\n'
                                , ctx)




    async def send_message(self, title, description, message):
        embed = discord.Embed(title=title, description=description, color=3426654)
        message = await message.channel.send(embed=embed)
        return message


def setup(client):
    client.add_cog(Commands(client))



