import discord
from discord.ext import commands
import mysql.connector
import asyncio
import os


class Leaderboard(commands.Cog):
    def __init__(self, client):
        self.client = client

    def connectDB(self):
        mydb = mysql.connector.connect(
            host="localhost",
            user="root",
            password="",
            database="practice_enterprise_bot"
        )
        return mydb

    db_kolom_wins = "wins"
    db_kolom_loses = "loses"
    db_kolom_ties = "ties"

    db_tabel_connect4 = "connect4"
    db_tabel_oxo = "oxo"

    amount_wins = 0
    amount_loses = 0
    amount_ties = 0
    win_rate = 0



    @commands.command(aliases=['lb', 'stats'])
    async def leaderboard(self, ctx):
        top_5_names = []

        leaderboard_array = self.total_leaderboard()
        if len(leaderboard_array) < 3:
            await ctx.send(f'ERROR: less than 3 people in leaderboard')
            return
        leaderboard_array = self.check_divide_by_zero(leaderboard_array)

        for i in range(len(leaderboard_array)):
            try:
                temp_var = await ctx.guild.fetch_member(leaderboard_array[i][0])
            except:
                temp_var = "User_not_in_server"
            finally:
                top_5_names.append(temp_var)



#        for i in range(len(leaderboard_array)):
#            temp_var = await ctx.guild.fetch_member(leaderboard_array[i][0])
#            top_5_names.append(temp_var)


        await self.send_message(f":first_place:\t{leaderboard_array[0][1]}W_{round((leaderboard_array[0][1] / leaderboard_array[0][2]) * 100)}%WR\t{top_5_names[0]}\n"
                                f':second_place:\t{leaderboard_array[1][1]}W_{round((leaderboard_array[1][1] / leaderboard_array[1][2]) * 100)}%WR\t{top_5_names[1]}\n'
                                f':third_place:\t{leaderboard_array[2][1]}W_{round((leaderboard_array[2][1] / leaderboard_array[2][2]) * 100)}%WR\t{top_5_names[2]}',
                                f'W = wins\nWR = winrate', ctx)

                                #voor 4de en 5de plaats:
                                    #f':four:\t{leaderboard_array[3][1]}W_{round((leaderboard_array[3][1] / leaderboard_array[3][2]) * 100)}%WR\t{top_5_names[3]}\n'
                                    #f':five:\t{leaderboard_array[4][1]}W_{round((leaderboard_array[4][1] / leaderboard_array[4][2]) * 100)}%WR\t{top_5_names[4]}'

    @commands.command()
    async def leaderboard_reset_connect4(self, ctx):
        self.reset_DB(self.db_tabel_connect4)

    @commands.command()
    async def leaderboard_reset_oxo(self, ctx):
        self.reset_DB(self.db_tabel_oxo)

    @commands.command(aliases=['leaderboard_4', 'lb_4', 'stats_4', 'stats_connect4'])
    async def leaderboard_connect4(self, ctx, player_to_search: discord.Member = None):
        db_tabel_used = self.db_tabel_connect4

        if player_to_search is None:
            player_to_search = ctx.author

        self.check_players_in_db(player_to_search, db_tabel_used)
        self.calculate_stats(player_to_search, db_tabel_used)

        await self.send_message(f"Player:\t\t{player_to_search}\nGame: \t\tConnect4\nWinrate:\t{self.win_rate}%",
                                f'Wins: {self.amount_wins}\nLoses: {self.amount_loses}\nTies: {self.amount_ties}', ctx)

        self.reset_var()

    @commands.command(aliases=['lb_oxo', 'stats_oxo'])
    async def leaderboard_oxo(self, ctx, player_to_search: discord.Member = None):
        db_tabel_used = self.db_tabel_oxo

        if player_to_search is None:
            player_to_search = ctx.author

        self.check_players_in_db(player_to_search, db_tabel_used)
        self.calculate_stats(player_to_search, db_tabel_used)

        await self.send_message(f"Player:\t\t{player_to_search}\nGame: \t\tOXO\nWinrate:\t{self.win_rate}%",
                                f'Wins: {self.amount_wins}\nLoses: {self.amount_loses}\nTies: {self.amount_ties}', ctx)

        self.reset_var()

    def get_stats_from_db(self, player, db_kolom, db_tabel):

        mydb = self.connectDB()
        my_cursor = mydb.cursor()
        my_sql = f"SELECT {db_kolom} FROM {db_tabel} WHERE user_id = {player}"
        my_cursor.execute(my_sql)
        myresult = my_cursor.fetchall()[0][0]

        my_cursor.close()
        mydb.close()

        return myresult

    def check_players_in_db(self, player, db_tabel):
        mydb = self.connectDB()
        my_cursor = mydb.cursor()
        my_player = player.id
        my_sql = f"SELECT user_id FROM {db_tabel} WHERE user_id = {my_player}"
        my_cursor.execute(my_sql)
        myresult = my_cursor.fetchall()
        if myresult == []:
            no_player = f"INSERT INTO {db_tabel} (user_id, wins, loses, ties) VALUES (%s, %s, %s, %s)"
            record = (my_player, 0, 0, 0)
            my_cursor.execute(no_player, record)
            mydb.commit()

        my_cursor.close()
        mydb.close()

    def calculate_stats(self, player_to_search, db_tabel):
        self.amount_wins = self.get_stats_from_db(player_to_search.id, self.db_kolom_wins, db_tabel)
        self.amount_loses = self.get_stats_from_db(player_to_search.id, self.db_kolom_loses, db_tabel)
        self.amount_ties = self.get_stats_from_db(player_to_search.id, self.db_kolom_ties, db_tabel)
        if (self.amount_wins + self.amount_ties + self.amount_loses) == 0:
            self.win_rate = 0
        else:
            self.win_rate = round((self.amount_wins / (self.amount_wins + self.amount_ties + self.amount_loses)) * 100)

    def reset_var(self):
        self.amount_wins = 0
        self.amount_loses = 0
        self.amount_ties = 0
        self.win_rate = 0

    def reset_DB(self, db_tabel):
        mydb = self.connectDB()
        my_cursor = mydb.cursor()
        my_sql = f"SELECT user_id FROM {db_tabel}"
        my_cursor.execute(my_sql)

        myresult = my_cursor.fetchall()
        for i in range(len(myresult)):
            id_user_to_alter = myresult[i][0]
            update_player_wins = f"UPDATE {db_tabel} SET {self.db_kolom_wins} = 0 WHERE user_id = {id_user_to_alter}"
            my_cursor.execute(update_player_wins)
            update_player_loses = f"UPDATE {db_tabel} SET {self.db_kolom_loses} = 0 WHERE user_id = {id_user_to_alter}"
            my_cursor.execute(update_player_loses)
            update_player_ties = f"UPDATE {db_tabel} SET {self.db_kolom_ties} = 0 WHERE user_id = {id_user_to_alter}"
            my_cursor.execute(update_player_ties)

            mydb.commit()

        my_cursor.close()
        mydb.close()

    def total_leaderboard(self):
        mydb = self.connectDB()
        my_cursor = mydb.cursor()
        my_sql = f"SELECT user_id, SUM(wins) AS total_wins, SUM(loses + wins + ties) AS total_games_played " \
                 f"FROM (SELECT	* FROM connect4 UNION ALL SELECT * FROM oxo ) total GROUP BY user_id ORDER BY total_wins DESC, total_games_played"
        my_cursor.execute(my_sql)
        myresult = my_cursor.fetchall()

        my_cursor.close()
        mydb.close()

        return myresult

    def check_divide_by_zero(self, array):
        x = list(array)

        for i in range(len(x)):
            x[i] = list(x[i])
            if x[i][2] == 0:
                x[i][2] = 1

        return x




    async def send_message(self, title, description, message):
        embed = discord.Embed(title=title, description=description, color=3426654)
        message = await message.channel.send(embed=embed)
        return message


def setup(client):
    client.add_cog(Leaderboard(client))
