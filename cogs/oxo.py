import discord
from discord.ext import commands
import mysql.connector



class Oxo(commands.Cog):
    def __init__(self, client):
        self.client = client

    def connectDB(self):
        mydb = mysql.connector.connect(
            host="localhost",
            user="root",
            password="",
            database="practice_enterprise_bot"
        )
        return mydb

    db_tabel_used = "oxo"

    oChar = ":o:"
    xChar = ":x:"
    playChar = ""
    board = []
    printboard = []
    gameAlreadyPlaying = False
    player1 = ""
    player2 = ""
    turn = ""
    message = None
    validcoordinates = ['a1', 'a2', 'a3', 'b1', 'b2', 'b3', 'c1', 'c2', 'c3']


    @commands.command()
    async def oxo(self, ctx, p2 : discord.Member = None):

        if self.gameAlreadyPlaying:
            await ctx.channel.send(f'{self.player1} and {self.player2} are already playing, wait for them to finish!')
            return

        if p2 is None:
            await ctx.channel.send("Enter a user to play with:  .oxo @name")
            return

        if p2 == ctx.author:
            await ctx.channel.send(f'You cant play against yourself!')
            return

        self.player1 = ctx.author
        self.player2 = p2
        self.turn = self.player1

        await ctx.channel.send(f'Use the command .oxo_place or .oxo_p to make a move.\n\tExample: .oxo_p a3\nTo reset the game use command: .oxo_reset or .oxo_r')

        self.board = self.create_board()
        self.printboard = self.array_print_board(self.board)
        self.message = await ctx.channel.send(self.string_print_board(self.printboard))

        self.check_players_in_db(self.db_tabel_used)

    @commands.command(aliases=['oxo_r'])
    async def oxo_reset(self, ctx):
        await self.message.delete()
        await ctx.channel.send(f'Game has been reset!')
        self.reset_game()


    @commands.command(aliases=['oxo_p'])
    async def oxo_place(self, ctx, coordinates = None):
        if self.gameAlreadyPlaying is False:
            return

        if coordinates not in self.validcoordinates:
            await ctx.channel.send(f'Enter a valid coordinate')
            return

        if ctx.author != self.player1 and ctx.author != self.player2:
            await ctx.channel.send(f'You are not a player. Only {self.player1} or {self.player2} can make a move!')
            return

        if ctx.author != self.turn:
            await ctx.channel.send(f'Its {self.turn} his turn')
            return


        if self.turn == ctx.author:
            if self.turn == self.player1:
                self.playChar = ":o:"
                self.turn = self.player2
            else:
                self.playChar = ":x:"
                self.turn = self.player1
        zet = self.coordinates_to_zet(coordinates)
        if self.illegal_move(zet):
            await ctx.channel.send(f'Illegal move, try again')
            return
        self.board[zet] = self.playChar
        self.printboard = self.array_print_board(self.board)
        await self.message.delete()
        self.message = await ctx.channel.send(self.string_print_board(self.printboard))

        if self.check_for_tie():
            await ctx.channel.send(f'Game has ended in a tie!')

            self.update_db(self.player1.id, "ties", self.db_tabel_used)
            self.update_db(self.player2.id, "ties", self.db_tabel_used)

            self.reset_game()
            return

        if self.winconditions_met():
            if self.turn == self.player1:
                await ctx.channel.send(f':trophy: {self.player2} won!')

                self.update_db(self.player2.id, "wins", self.db_tabel_used)
                self.update_db(self.player1.id, "loses", self.db_tabel_used)

            else:
                await ctx.channel.send(f':trophy: {self.player1} won!')

                self.update_db(self.player1.id, "wins", self.db_tabel_used)
                self.update_db(self.player2.id, "loses", self.db_tabel_used)

            self.reset_game()




    def create_board(self):
        temp_board = []

        for i in range(9):
            temp_board.append(':black_large_square:')
            self.gameAlreadyPlaying = True
        return temp_board

    def array_print_board(self, board):
        printBoard = [':flag_be:', ':one:', ':two:', ':three:', '\n', ':regional_indicator_a:']

        for i in range(9):
            if i == 3:
                printBoard.append('\n')
                printBoard.append(':regional_indicator_b:')
            elif i == 6:
                printBoard.append('\n')
                printBoard.append(':regional_indicator_c:')
            printBoard.append(board[i])

        return printBoard

    def string_print_board(self, printboard):
        printString = ""
        for i in range(len(printboard)):
            printString += printboard[i]

        return printString

    def coordinates_to_zet(self, coordinates):

        if coordinates == 'a1':
            return 0
        elif coordinates == 'a2':
            return 1
        elif coordinates == 'a3':
            return 2
        elif coordinates == 'b1':
            return 3
        elif coordinates == 'b2':
            return 4
        elif coordinates == 'b3':
            return 5
        elif coordinates == 'c1':
            return 6
        elif coordinates == 'c2':
            return 7
        elif coordinates == 'c3':
            return 8

    def winconditions_met(self):
        if self.board[0] == self.playChar and self.board[1] == self.playChar and self.board[2] == self.playChar:
            return True
        if self.board[3] == self.playChar and self.board[4] == self.playChar and self.board[5] == self.playChar:
            return True
        if self.board[6] == self.playChar and self.board[7] == self.playChar and self.board[8] == self.playChar:
            return True
        if self.board[0] == self.playChar and self.board[3] == self.playChar and self.board[6] == self.playChar:
            return True
        if self.board[1] == self.playChar and self.board[4] == self.playChar and self.board[7] == self.playChar:
            return True
        if self.board[2] == self.playChar and self.board[5] == self.playChar and self.board[8] == self.playChar:
            return True
        if self.board[0] == self.playChar and self.board[4] == self.playChar and self.board[8] == self.playChar:
            return True
        if self.board[2] == self.playChar and self.board[4] == self.playChar and self.board[6] == self.playChar:
            return True

    def check_for_tie(self):
        check = 0
        for i in range(len(self.board)):
            if self.board[i] != ':black_large_square:':
                check += 1
        if check == len(self.board):
            return True

    def reset_game(self):

        self.playChar = ""
        self.board = []
        self.printboard = []
        self.gameAlreadyPlaying = False
        self.player1 = ""
        self.player2 = ""
        self.turn = ""
        self.message = None

    def illegal_move(self, zet):
        if self.board[zet] != ':black_large_square:':
            if self.turn == self.player1:
                self.turn = self.player2
            else:
                self.turn = self.player1
            return True

    def update_db(self, player, db_kolom, db_tabel):

        mydb = self.connectDB()
        my_cursor = mydb.cursor()
        my_sql = f"SELECT {db_kolom} FROM {db_tabel} WHERE user_id = {player}"
        my_cursor.execute(my_sql)

        myresult = my_cursor.fetchall()[0][0]
        update_player = f"UPDATE {db_tabel} SET {db_kolom} = {myresult + 1} WHERE user_id = {player}"
        my_cursor.execute(update_player)
        mydb.commit()
        my_cursor.close()
        mydb.close()

    def check_players_in_db(self, db_tabel):
        mydb = self.connectDB()
        my_cursor = mydb.cursor()
        my_player = self.player1.id
        my_sql = f"SELECT user_id FROM {db_tabel} WHERE user_id = {my_player}"
        my_cursor.execute(my_sql)
        myresult = my_cursor.fetchall()
        if myresult == []:
            no_player = f"INSERT INTO {db_tabel} (user_id, wins, loses, ties) VALUES (%s, %s, %s, %s)"
            record = (my_player, 0, 0, 0)
            my_cursor.execute(no_player, record)
            mydb.commit()

        my_player = self.player2.id
        my_sql = f"SELECT user_id FROM {db_tabel} WHERE user_id = {my_player}"
        my_cursor.execute(my_sql)
        myresult = my_cursor.fetchall()
        if myresult == []:
            no_player = f"INSERT INTO {db_tabel} (user_id, wins, loses, ties) VALUES (%s, %s, %s, %s)"
            record = (my_player, 0, 0, 0)
            my_cursor.execute(no_player, record)
            mydb.commit()
        my_cursor.close()
        mydb.close()



def setup(client):
    client.add_cog(Oxo(client))








