import discord
from discord.ext import commands
import asyncio

class Votekick(commands.Cog):
    def __init__(self, client):
        self.client = client

    busy = False

    async def send_message(self, title, description, message):
        embed = discord.Embed(title=title, description=description, color=3426654)
        message = await message.channel.send(embed=embed)
        return message



    @commands.command()
    async def votekick(self, ctx, member : discord.Member = None):

        if self.busy:
            await ctx.channel.send("Votekick already in use!")
            return
        else: self.busy = True

        if member is None:
            await ctx.channel.send("Enter a user to kick!")
            self.busy = False
            return

        if member.voice is None:
            await ctx.channel.send(f'{member.display_name} is not in a voice channel!')
            self.busy = False
            return



        msg = await self.send_message("Votekick", f'Do you want to votekick {member.display_name}', ctx)
        await msg.add_reaction('\N{THUMBS UP SIGN}')
        await msg.add_reaction('\N{THUMBS DOWN SIGN}')

        await asyncio.sleep(15)


        msg = await ctx.channel.fetch_message(msg.id)
        if msg.reactions[0].count > msg.reactions[1].count:
            await member.edit(voice_channel=None)

        await msg.delete()

        self.busy = False


            #temp_channel = await ctx.guild.create_voice_channel("Temp")
            #await member.move_to(temp_channel)
            #await temp_channel.delete()








def setup(client):
    client.add_cog(Votekick(client))